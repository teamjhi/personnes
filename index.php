<?php
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";

// Autoloader des classes
require_once WAY . '/class/autoloader.inc.php';

// Securité
$autorisation_str = "PER_ADM";
require WAY . '/secure.inc.php';

// Nouveau header
require(WAY . "/include/header.inc.php");

//$scripts_list = [''];
//require_once WAY . '/include/scripts.inc.php';

/*** NavBar *******************************************************/
$back = ROOT . "index.php?mnu=15";
require(WAY . "/include/navbar.inc.php");
/*** NavBar *******************************************************/
$classe= new Classe();
$tab_ens = $classe->get_tab_ens();
?>
</head>
<div class="container">
    <div class="col-md-12 main">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                <div class="col-md-2">
                    <h3 class="panel-title">Enseignants</h3>
                </div>
                <div class="col-md-3 col-md-offset-7">
                    <a href="add_ens.php"><button class="btn btn-xs btn-default">Ajouter un(e) enseignant(e)</button></a>
                </div>
            </div>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <?php
                    echo "<table class=\"table table-condensed table-responsive table-hover table-striped table-bordered\">";
                    foreach ($tab_ens AS $ens) {
                        if (!$ens['actif_per']) {
                            $classe = "inactif";
                        } else {
                            $classe = "";
                        }
                        echo "<tr class=\"" . $classe . "\">";
                        echo "<td>" . $ens['nom_per'] . "</td>";
                        echo "<td>" . $ens['prenom_per'] . "</td>";
                        echo "<td>" . $ens['initial_per'] . "</td>";
                        echo "<td>" . $ens['email_per'] . "</td>";
                        echo "<td>";
                        echo "<form method=\"post\" action=\"add_ens.php\">";
                        echo "<input type=\"submit\"  class=\"btn btn-xs btn-primary\"value=\"Modifier\">";
                        echo "<input type=\"hidden\" name=\"id_per\" value=\"" . $ens['id_per'] . "\">";
                        echo "</form>";
                        echo "</td>";
                        echo "<td>";

                        echo "<form method=\"post\"  id=\"desactive_per_" . $ens['id_per'] . "\" action=\"" . $_SERVER['PHP_SELF'] . "\">";
                        if ($ens['actif_per']) {
                            echo "<input type=\"button\" class=\"btn btn-xs btn-warning\" onClick=\"confirmation(" . $ens['id_per'] . ");\" value=\"Désactiver\"> ";
                            echo "<input type=\"hidden\" name=\"actif_per\" value=\"0\">";
                        } else {
                            echo "<input type=\"hidden\" name=\"actif_per\" value=\"1\">";
                            echo "<input type=\"button\" class=\"btn btn-xs btn-success\" onClick=\"confirmation(" . $ens['id_per'] . ");\" value=\"Activer\"> ";
                        }
                        echo "<input type=\"hidden\" name=\"disable_per\" value=\"" . $ens['id_per'] . "\">";
                        echo "</form>";


                        echo "</td>";
                        echo "</tr>";
                    }
                    echo "</table>";
                    ?>
                </div> <!-- panel-body -->
            </div> <!-- panel-body -->
        </div> <!-- panel -->
    </div> <!-- main -->
</div>
</body>
</html>
