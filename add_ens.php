<?php
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";

// Autoloader des classes
require_once WAY . '/class/autoloader.inc.php';

// Securité
$autorisation_str = "PER_ADM";
require WAY . '/secure.inc.php';

// Nouveau header
require(WAY . "/include/header.inc.php");

$scripts_list = [''];
require_once WAY . '/include/scripts.inc.php';

echo "<script src=\"https://cdn.jsdelivr.net/npm/jquery-validation@1.19.1/dist/jquery.validate.js\"></script>";

/*** NavBar *******************************************************/
$back = ROOT . "index.php?mnu=15";
require(WAY . "/include/navbar.inc.php");
/*** NavBar *******************************************************/
$commune = new Communes();
$pay = new Pays();
$tab_pays = $pay->get_all_indicatifs();

$dip = new Diplome();

$classe = new Classe();
$tab_all_brg = $classe->get_tab_branches_generiques();


if(isset($_POST['id_per'])){
    $per = new Personne($_POST['id_per']);
    $modif = true;
}else{
    $per = new Personne();
}

$tab_brg = $per->get_all_brg();
$tab_fct = $per->get_all_fct();
$tab_dip = $per->get_all_dip();

$tab_all_dip = $dip->get_all();

if(!$modif){
    $title = "Formulaire d'ajout d'enseignants";
}else{
    $title = "Formulaire de modification d'enseignants";
}

//echo '<pre>';
//print_r($tab_brg);
//echo '</pre>';


?>
</head>
<div class="container">

    <div class="col-md-12 main">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><?=$title?></h3>
            </div>
            <div class="panel-body">

                <!---------- Debut formulaire ---------->
                <input type="hidden" id="id_per" id_per="<?=$per->get_id()?>">
                <form id="inscription_form">

                    <!---------- Premier pack ---------->
                    <div>
                        <div class="form-group row">

                            <!-- Nom -->
                            <label for="nom_per" class="col-sm-2 col-form-label" >Nom</label>
                            <div class="col-sm-3">
                                <input class="form-control" id="nom_per" type="text" name="nom_per" value="<?= $per->get_nom()?>">
                            </div>

                            <!-- Adresse -->
                            <label for="rue_per" class="col-sm-2 col-form-label" >Adresse</label>
                            <div class="col-sm-3">
                                <input class="form-control" id="rue_per" type="text" name="adresse_per" value="<?= $per->get_rue()?>">
                            </div>
                        </div>
                    </div>

                    <div class="">
                        <div class="form-group row">

                            <!-- Prenom -->
                            <label for="prenom_per" class="col-sm-2 col-form-label" >Prénom</label>
                            <div class="col-sm-3">
                                <input class="form-control" id="prenom_per" type="text" name="prenom_per" value="<?= $per->get_prenom()?>">
                            </div>

                            <!-- Localite -->
                            <label for="id_cmn" class="col-sm-2 col-form-label">Localité</label>
                            <div class="col-sm-3">
                                <?php
                                if ($per->get_id_cmn() != null AND $per->get_id_cmn() != 0) {
                                    $cmn = $commune->get_cmn_per($per->get_id_cmn());
                                    echo '<input type="text" class="form-control select_commune" id_item="" placeholder="Localité" value="' . $cmn['nom_ville_cmn'] . " " . $cmn['npa_cmn'] . '">';
                                    echo '<input type="hidden" name="id_cmn" id="id_cmn" class="id_cmn_ form-control" value=' . $per->get_id_cmn() . '>';
                                } else {
                                    echo '<input type="text" class="form-control select_commune" id_item="" placeholder="NPA Localité">';
                                    echo '<input type="hidden" name="id_cmn" id="id_cmn" class="id_cmn_ form-control">';
                                } ?>
                            </div>

                        </div>
                    </div>

                    <div class="">
                        <div class="form-group row">

                            <!-- Abreviation -->
                            <label for="initial_per" class="col-sm-2 col-form-label" >Abréviation</label>
                            <div class="col-sm-3">
                                <input class="form-control" id="initial_per" type="text" name="initial_per" value="<?= $per->get_initial()?>">
                            </div>

                            <!-- Genre -->
                            <label for="genre_per" class="col-sm-2 col-form-label" >Genre</label>
                            <div class="col-sm-2">
                                <select class="form-control" id="genre_per">
                                    <option Value="H" <?php if($per->get_genre()=="H") echo "selected"?>>Homme</option>
                                    <option Value="F" <?php if($per->get_genre()=="F") echo "selected"?>>Femme</option>
                                    <option Value="A" <?php if($per->get_genre()=="A") echo "selected"?>>Autre</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <hr>


                    <!---------- Deuxieme pack ---------->

                    <!---- Ligne 1 ---->
                    <div>
                        <div class="form-group row">

                            <!-- Telephone mobile -->
                            <label for="tel_mob_per" class="col-sm-2 col-form-label">Tel. Mobile</label>
                            <div class="col-sm-1">
                                <select class="form-control tel_ens_form" id="ind_mob_per">
                                    <?php
                                    foreach($tab_pays as $pays){
                                        if($per->get_indicatif_mobile() != null){
                                            echo "\t\t\t\t\t\t\t\t\t<option value='".$pays['id_pay']."' ".(($pays['indicatif_pay'] == $per->get_indicatif_mobile())? "selected" : "" ).">+".$pays['indicatif_pay']."</option>\n";
                                        }else {
                                            echo "\t\t\t\t\t\t\t\t\t<option value='" . $pays['id_pay'] . "' " . (($pays['indicatif_pay'] == 41) ? "selected" : "") . ">+" . $pays['indicatif_pay'] . "</option>\n";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" id="tel_mob_per" type="text" name="tel_mob_per" value="<?= $per->get_telephone_mobile()?>" placeholder="(0) 79 729 09 09">
                            </div>

                            <!-- Email 1 -->
                            <label for="email_per" class="col-sm-2 col-form-label" >E-mail 1 (identifiant)</label>
                            <div class="col-sm-3">
                                <input class="form-control" id="email_per" type="text" name="email_per" value="<?= $per->get_email()?>">
                            </div>
                        </div>
                    </div>

                    <!---- Ligne 2 ---->
                    <div>
                        <div class="form-group row">

                            <!-- Telephone prive -->
                            <label for="tel_per" class="col-sm-2 col-form-label">Tel. Privé</label>
                            <div class="col-sm-1">
                                <select class="form-control tel_ens_form" id="ind_per">
                                    <?php
                                    foreach($tab_pays as $pays){
                                        echo "\t\t\t\t\t\t\t\t\t<option value='".$pays['id_pay']."' ".(($pays['indicatif_pay'] == 41)? "selected" : "" ).">+".$pays['indicatif_pay']."</option>\n";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" id="tel_per" type="text" name="tel_per" value="<?= $per->get_telephone()?>" placeholder="">
                            </div>

                            <!-- Email 2 -->
                            <label for="email_2_per" class="col-sm-2 col-form-label" >E-mail 2</label>
                            <div class="col-sm-3">
                                <input class="form-control modif_per" id="email_2_per" type="text" name="email_2_per" value="<?= $per->get_email_2()?>">
                            </div>
                        </div>
                    </div>

                    <!---- Ligne 3 ---->
                    <div>
                        <div class="form-group row">
                            <!-- Telephone prof -->
                            <label for="tel_emp_per" class="col-sm-2 col-form-label">Tel. Professionel</label>
                            <div class="col-sm-1">
                                <select class="form-control tel_ens_form" id="ind_emp_per">
                                    <?php
                                    foreach($tab_pays as $pays){
                                        echo "\t\t\t\t\t\t\t\t\t<option value='".$pays['id_pay']."' ".(($pays['indicatif_pay'] == 41)? "selected" : "" ).">+".$pays['indicatif_pay']."</option>\n";
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-2">
                                <input class="form-control" id="tel_emp_per" type="text" name="tel_emp_per" value="<?= $per->get_telephone_employeur()?>" placeholder="">
                            </div>
                        </div>
                    </div>

                    <hr>


                    <!---------- Troisieme pack ---------->
                    <!---- Ligne 1 ---->
                    <div class="form-group row">
                        <label for="date_per" class="col-sm-2 col-form-label">Date de naissance</label>
                        <!-- date de naissance -->
                        <div class="col-sm-2">
                            <div class="input-group" id="date_naissance_per_group">
                                <label for="date_naissance_per" id="label_datepicker_birth" class="input-group-addon" data-toggle="tooltip" data-placement="bottom">
                                    <i class="glyphicon glyphicon-calendar"></i>
                                </label>
                                <input type="text" class="form-control datepicker small_input" id="date_naissance_per" name="date_naissance_per" value="<?= date("d.m.Y",$per->get_date_naissance())?>">
                            </div>
                        </div>
                        <!-- Num AVS -->
                        <label for="no_avs_per" class="col-sm-1 col-form-label">N° AVS</label>
                        <div class="col-sm-2">
                            <input class="form-control small_input" type="text" id='no_avs_per' name="no_avs_per" value="<?= $per->get_no_avs()?>" placeholder="756.0000.0000.00">
                        </div>
                        <!-- Num Persiska -->
                        <label for="no_persiska_per" class="col-sm-2 col-form-label">N° Persiska</label>
                        <div class="col-sm-2">
                            <input class="form-control small_input" type="text" id='no_persiska_per' name="no_persiska_per" value="<?= $per->get_no_persiska()?>" placeholder="000.000">
                        </div>
                    </div>

                    <!---- Ligne 2 ---->
                    <div class="form-group row">
                        <label for="remarque_ens_per" class="col-sm-2 col-form-label" >Remarques</label>
                        <div class="col-sm-4">
                            <textarea class="form-control" id="remarque_ens_per" name="remarque_per" placeholder="Ecrivez vos remarques"><?= $per->get_remarque_ens()?></textarea>
                        </div>
                    </div>

                    <div id="btn_group">
                        <?php
                            if($modif){
                                echo "<div class=\"col-md-offset-8 col-md-2\"><input type=\"button\" class=\"form-control btn btn-primary submit\" id=\"modif_conf\" value=\"Modifier\"></div>";
                            }else{
                                echo "<div class=\"col-md-offset-8 col-md-2\"><input type=\"submit\" class=\"form-control btn btn-primary submit\" id=\"submit_conf\" value=\"Ajouter\"></div>";
                                echo "<div id='btn_modif_ens' style='display: none' class=\"col-md-offset-8 col-md-2\"><input type=\"button\" class=\"form-control btn btn-primary submit\" id=\"modif_conf\" value=\"Modifier\"></div>";
                            }
                        ?>
                        <div class="col-md-2">
                            <input type="reset" class="form-control btn btn-warning" id="reset_conf" value="Annuler">
                        </div>
                    </div>

                    <hr>

                    <!---------- Quatrieme pack ---------->
                    <!------ Engagements / Fonctions ------>
                    <div id="fct_brg_dip">
                    <span id="fonctions" class="col-md-12">
                        <div class="col-md-offset-1"><h4>Engagements / Fonctions</h4></div>
                        <table class="table table-condensed" id="tbl_fonction">
                            <tr>
                                <th class="col-md-2">Début</th>
                                <th class="col-md-2">Fin</th>
                                <th class="col-md-2">Fonction</th>
                                <th class="col-md-1"></th>
                            </tr>
                            <?php
                            //TODO:AJOUTER "ID_PER_FCT" --> <TR> & <BTN>
                            foreach ($tab_fct as $fonction){
                                echo "<tr id=\"fct_per_". $fonction['id_per_fct']."\">";
                                echo "<td>".$fonction['date_debut']."</td>";
                                if($fonction['date_fin'] == "0000-00-00"){
                                    echo "<td>-</td>";
                                }else {
                                    echo "<td>" . $fonction['date_fin'] ."</td>";
                                }
                                echo "<td>".$fonction['nom_fct']."</td>";
                                echo "<td><button type='button' class='btn btn-xs btn-danger btn_remove_fct' id_per_fct=\"".$fonction['id_per_fct']."\"><span class='glyphicon glyphicon-remove' aria-hidden='true'></span></button></td>";
                                echo "</tr>";
                            }
                            ?>
                            <tr class="form_line">
                                <td>
                                    <div class="input-group" id="inputG_date_deb">
                                        <label for="date_debut_fct" id="label_datepicker_debut_fct" class="input-group-addon" data-toggle="tooltip" data-placement="bottom" title="Semaine du">
					                        <i class="glyphicon glyphicon-calendar"></i>
					                    </label>
                                        <input type="text" class="form-control datepicker" id="date_debut_fct" value="">
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group">
                                        <label for="date_fin_fct" id="label_datepicker_fin_fct" class="input-group-addon" data-toggle="tooltip" data-placement="bottom" title="Semaine du">
					                        <i class="glyphicon glyphicon-calendar"></i>
					                    </label>
                                        <input type="text" class="form-control datepicker" id="date_fin_fct" value="">
                                    </div>
                                </td>
                                <td>
                                    <select class="form-control" id="id_fct">
                                        <?php
                                            $fct = new Fonction();
                                            $tab_fct = $fct->get_all();
                                            foreach($tab_fct as $fc){
                                                echo '<option nom_fct="'.$fc['nom_fct'].'" value="'.$fc['id_fct'].'">';
                                                echo $fc['nom_fct'];
                                                echo '</option>';
                                            }
                                        ?>
                                    </select>
                                </td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-success" id="btn_add_fct">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                    </button>
                                    <button type="button" class="btn btn-xs btn-warning" id="btn_cancel_fct">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </button>
                                </td>
                                <td class="col-sm-3" id="error_show_fct">

                                </td>
                            </tr>

                        </table>
                </span>
                <!------ Fin Engagements / Fonctions ------>

                    <hr>

                    <br>
                    <br>

                    <div id="branches" class="col-sm-8">
                        <h4>Branches</h4>
                        <table class="table table" id="brn">
                            <?php
                            foreach ($tab_brg as $branche){
                                echo "<tr id=\"brg_per_".$branche['id_per_brg']."\">";
                                echo "<td></td>";
                                echo "<td>".$branche['nom_brg']."</td>";
                                echo "<td>";
                                echo "<button type=\"button\" class=\"btn btn-xs btn-danger btn_remove_brn\" id_brg_per=\"".$branche['id_per_brg']."\">";
                                echo "<span class='glyphicon glyphicon-remove' aria-hidden='true'></span>";
                                echo "</button>";
                                echo "</td>";
                                echo "</tr>";
                                }
                            ?>
                            <tr id="header_brn">
                                <th></th>
                                <th class="col-md-10">
                                    <div>
                                        <select id="id_brg" class="form-control">
                                            <?php
                                            foreach($tab_all_brg as $brn){
                                                echo "<option nom_brg=\"".$brn['nom_brg']."\" value='";
                                                echo $brn['id_brg'];
                                                echo "'>";
                                                echo $brn['nom_brg'];
                                                echo "</option>\n";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </th>
                                <th>

                                    <div class="form-group col-sm-4" id="btn_add_brn">
                                        <button type="button" class="btn btn-xs btn-primary">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </div>
                                </th>


                            </tr>
                        </table>
                    </div>


                    <div id="diplomes" class="col-sm-8">
                        <h4>Diplômes</h4>
                        <table class="table table">
                            <?php
                            foreach ($tab_dip as $diplome){
                                echo "<tr id=\"dip_per_".$diplome['id_per_dip']."\">";
                                echo "<td></td>";
                                echo "<td>".$diplome['nom_dip']."</td>";
                                echo "<td>";
                                echo "<button type=\"button\" class=\"btn btn-xs btn-danger btn_remove_dip\" id_dip_per=\"".$diplome['id_per_dip']."\">";
                                echo "<span class='glyphicon glyphicon-remove' aria-hidden='true'></span>";
                                echo "</button>";
                                echo "</td>";
                                echo "</tr>";
                            }
                            ?>
                            <tr id="header_dip">
                                <th></th>
                                <th>
                                    <div>
                                        <select id="id_dip" class="form-control">
                                            <?php
                                            foreach($tab_all_dip as $dip){
                                                echo "<option nom_dip=\"".$dip['nom_dip']."\" value='";
                                                echo $dip['id_dip'];
                                                echo "'>";
                                                echo $dip['nom_dip'];
                                                echo "</option>\n";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </th>
                                <th>
                                    <div class="form-group col-sm-4">
                                        <button id=btn_add_dip type="button" class="btn btn-xs btn-primary">
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                    </div>
                                </th>
                            </tr>
                        </table>
                    </div>
                    </div>
</form>


            </div>
        </div> <!-- panel-body -->
    </div> <!-- panel -->
</div> <!-- main -->


</div>
</body>
<script src="js/fct_brg_dip.js"></script>
<script src="js/regles_formulaire.js"></script>
</html>
