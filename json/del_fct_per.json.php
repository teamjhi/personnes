<?php
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";

// Autoloader des classes
require_once WAY . '/class/autoloader.inc.php';

// Securité
$autorisation_str = "PER_ADM";
require WAY . '/secure.inc.php';

$per = new Personne();

$del = $per->del_per_fct($_POST['id_per_fct']);

if($del) {
    $rslt['reponse'] = true;
    $rslt['data'] = $_POST;
}else{
    $rslt['reponse'] = false;
    $rslt['data'] = $_POST;
}

echo json_encode($rslt);