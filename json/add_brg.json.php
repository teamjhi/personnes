<?php
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";

// Autoloader des classes
require_once WAY . '/class/autoloader.inc.php';

// Securité
$autorisation_str = "PER_ADM";
require WAY . '/secure.inc.php';

$per = new Personne();

$tab = $_POST;

$id = $per->add_per_brg($tab);

if(isset($id)) {
    $rslt['reponse'] = true;
    $rslt['data'] = $tab;
    $rslt['id'] = $id;
}else{
    $rslt['reponse'] = false;
    $rslt['data'] = $tab;
}

echo json_encode($rslt);