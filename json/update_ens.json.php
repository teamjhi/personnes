<?php
session_start();
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require("../../../../config/config.php");
include(WAY . "/class/class.inc.php");
include(WAY . "/include/function.inc.php");
$autorisation_str = "PER_ADM";
require(WAY . "/secure.inc.php");

$personne = new Personne();

$tab = $_POST;

$tab['date_naissance_per'] = date("Y-m-d", strtotime($tab['date_naissance_per']));

$modif = $personne->update_ens($tab);

if($modif) {
    $rslt['reponse'] = true;
    $rslt['data'] = $tab;
}else{
    $rslt['reponse'] = false;
    $rslt['data'] = $tab;
}

echo json_encode($rslt);