<?php
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";

// Autoloader des classes
require_once WAY . '/class/autoloader.inc.php';

// Securité
$autorisation_str = "PER_ADM";
require WAY . '/secure.inc.php';

$per = new Personne();

$date = new DateTime($_POST['date_per']);
//date("Y-m-d", strtotime($_POST['date_per']));
$rlt = $date->format("Y-m-d");

//Values
$tab_ens['date_naissance_per'] = date("Y-m-d", strtotime($_POST['date_naissance_per'])) ;
$tab_ens['email_per'] = $_POST['email_per'];
$tab_ens['email_2_per'] = $_POST['email_2_per'];
$tab_ens['genre_per'] = $_POST['genre_per'];
$tab_ens['id_cmn'] = $_POST['id_cmn'];
$tab_ens['initial_per'] = $_POST['initial_per'];
$tab_ens['no_avs_per'] = $_POST['no_avs_per'];
$tab_ens['no_persiska_per'] = $_POST['no_persiska_per'];
$tab_ens['nom_per'] = $_POST['nom_per'];
$tab_ens['prenom_per'] = $_POST['prenom_per'];
$tab_ens['remarque_ens_per'] = $_POST['remarque_ens_per'];
$tab_ens['rue_per'] = $_POST['rue_per'];
$tab_ens['tel_emp_per'] = $_POST['ind_emp'] . " " . $_POST['tel_emp_per'];
$tab_ens['tel_mob_per'] = $_POST['ind_mob'] . " " . $_POST['tel_mob_per'];
$tab_ens['tel_per'] = $_POST['ind_tel'] . " " . $_POST['tel_per'];

if (substr($_POST['tel_emp_per'], 0, 1) == "0") {
    $tab_ens['tel_emp_per'] = $_POST['ind_emp'] . " " . substr($_POST['tel_emp_per'], 1);
}
if (substr($_POST['tel_mob_per'], 0, 1) == "0") {
    $tab_ens['tel_mob_per'] = $_POST['ind_mob'] . " " . substr($_POST['tel_mob_per'], 1);
}
if (substr($_POST['tel_per'], 0, 1) == "0") {
    $tab_ens['tel_per'] = $_POST['ind_tel'] . " " . substr($_POST['tel_per'], 1);
}

$id = $per->add($tab_ens);

if (isset($id)){
    $per = new Personne($id);
    $result['reponse'] = true;
    $result['datas'] = $tab_ens;
    $result['id'] = $id;

}else {
    $result['reponse'] = false;
    $result['datas'] = $tab_ens;
}
echo json_encode($result);