<?php
session_start();
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";
require_once WAY . '/class/autoloader.inc.php';
include(WAY . "/include/function.inc.php");
$autorisation_str = "PER_ADM";
require_once WAY . "/secure.inc.php";

$communes = new Communes();
$tab_cmn = $communes->get_filter_cmn($_GET['term']);
echo json_encode($tab_cmn);