<?php
header('Content-type: text/json');
header('Content-type: application/json; charset=utf-8');
session_start();
require_once substr(__dir__, 0, strpos(__dir__, "portail-ef")) . "/config/config.php";

// Autoloader des classes
require_once WAY . '/class/autoloader.inc.php';

// Securité
$autorisation_str = "PER_ADM";
require WAY . '/secure.inc.php';

$per = new Personne();

$tab = $_POST;

$tab['date_debut'] = date("Y-m-d", strtotime($tab['date_debut']));
if($tab['date_fin'] != ""){
    $tab['date_fin'] = date("Y-m-d", strtotime($tab['date_fin']));
}

$id = $per->add_per_fct($tab);

if(isset($id)) {
    $tab['date_debut'] = date("d.m.Y", strtotime($tab['date_debut']));
    if($tab['date_fin'] != "") {
        $tab['date_fin'] = date("d.m.Y", strtotime($tab['date_fin']));
    }

    $rslt['reponse'] = true;
    $rslt['data'] = $tab;
    $rslt['id'] = $id;
}else{
    $rslt['reponse'] = false;
    $rslt['data'] = $tab;
}

echo json_encode($rslt);