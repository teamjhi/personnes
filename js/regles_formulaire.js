$(function () {
    modif();
    $(".select_commune").autocomplete({
        source: "./json/liste_cmn.json.php",
        minLength: 3,
        select: function (event, ui) {
            $('.id_cmn_' + $(this).attr("id_item")).val(ui.item.id);
        }
    });

    function modif(){
        $isModif = false;
        if($('#id_per').attr('id_per')!=""){
            $('#fct_brg_dip').css('display','block');
            $isModif = true;
        }
    }

    $('#modif_conf').on('click',function () {
        if($('#tel_emp_per').val() != ""){
            var var_tel_emp_per = $('#tel_emp_per').val();
            var var_ind_emp_per = $('#ind_emp_per option:selected').text();
        }else{
            var var_tel_emp_per = "";
            var var_ind_emp_per = "";
        }

        if($('#tel_mob_per').val() != ""){
            var var_tel_mob_per = $('#tel_mob_per').val();
            var var_ind_mob_per = $('#ind_mob_per option:selected').text();
        }else{
            var var_tel_mob_per = "";
            var var_ind_mob_per = "";
        }

        if($('#tel_per').val() != ""){
            var var_tel_per = $('#tel_per').val();
            var var_ind_per = $('#ind_per option:selected').text();
        }else{
            var var_tel_per = "";
            var var_ind_per = "";
        }

        $.post(
            "./json/update_ens.json.php?_=" + Date.now(),
            {
                id_per: $('#id_per').attr('id_per'),
                nom_per: $('#nom_per').val(),
                prenom_per: $('#prenom_per').val(),
                rue_per: $('#rue_per').val(),
                id_cmn: $('#id_cmn').val(),
                initial_per: $('#initial_per').val(),
                genre_per: $('#genre_per').val(),
                date_naissance_per: $('#date_naissance_per').val(),
                email_per: $('#email_per').val(),
                email_2_per: $('#email_2_per').val(),
                remarque_ens_per: $('#remarque_ens_per').val(),
                no_avs_per: $('#no_avs_per').val(),
                no_persiska_per: $('#no_persiska_per').val(),
                tel_emp_per: var_tel_emp_per,
                ind_emp: var_ind_emp_per,
                tel_mob_per: var_tel_mob_per,
                ind_mob: var_ind_mob_per,
                tel_per: var_tel_per,
                ind_tel: var_ind_per,
            },function () {

            });
    })
    $("#inscription_form").validate(
        {
            rules:{
                nom_per:{
                    required:true,
                    minlength:2
                },
                prenom_per:{
                    required:true,
                    minlength:2
                },
                tel_mob_per:{
                    digits: true
                },
                tel_emp_per:{
                    digits: true
                },
                tel_per:{
                    digits: true
                },
                email_per:{
                    remote:{
                        url:"./chk/email_ens.chk.php?_=" + Date.now(),
                        data: {'email_per':function() {
                                if (!$isModif) {
                                    return $('#email_per').val()
                                }
                            }
                            },
                        type: "POST"
                    },
                    required:true,
                    email:true
                },
                email_2_per:{
                    remote:{
                        url:"./chk/email_ens.chk.php?_=" + Date.now(),
                        data: {'email_per':function() {
                                    return $('#email_per').val()
                            }
                        },
                        type: "POST"
                    },
                    email:true
                },
                no_persiska_per:{
                    required:true,
                    digits:true
                },
                no_avs_per:{
                    required:true,
                    digits:true
                },
                date_naissance_per:{
                    required:true
                }
            },
            messages: {
                nom_per: {
                    required:"Veuillez saisir votre nom",
                    minlength: "Votre nom doit être composé de 2 caractères au minimum"
                },
                prenom_per: {
                    required: "Veuillez saisir votre prénom",
                    minlength: "Votre prénom doit être composé de 2 caractères au minimum"
                },
                email_per: {
                    required: "Veuillez saisir votre adresse mail",
                    remote:  "Email déjà utilisé",
                    email: "Email incorrect"
                },
                email_2_per: {
                    remote:  "Email déjà utilisé",
                    email: "Email incorrect"
                },
                tel_mob_per: {
                    digits: "Numéro de Tel. incorrect",
                },
                tel_emp_per: {
                    digits: "Numéro de Tel. incorrect",
                },
                tel_per: {
                    digits: "Numéro de Tel. incorrect",
                },
                no_persiska_per: {
                    required: "Veuillez saisir votre numéro Persiska",
                    digits:"Numéro Persiska non valide"
                },
                no_avs_per: {
                    required: "Veuillez saisir votre numéro AVS",
                    digits:"Numéro AVS non valide"
                },
                date_naissance_per: {
                    required:"Veuillez saisir votre date de naissance",
                }

            },
            errorPlacement:function (error,element) {
                if(element.attr('id')=="date_naissance_per"){
                    error.insertAfter('#date_naissance_per_group');
                }else{
                    $(element.parent()).append(error);
                }
            },
            showErrors: function(errorMap, errorList) {
                errorList.forEach(function(error) {
                    var message = error.message;
                        error.message = $('<div class="col-sm-12 alert alert-danger alert_nom" role="alert">\n' +
                            '  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n' +
                            '  <span class="sr-only">Error:</span>\n' +
                            '  '+message+'\n' +
                            '</div>');
                });
                this.defaultShowErrors();
            },
            errorClass:'col-sm-12 error',

            submitHandler:function (form) {

                if($('#tel_emp_per').val() != ""){
                    var var_tel_emp_per = $('#tel_emp_per').val();
                    var var_ind_emp_per = $('#ind_emp_per option:selected').text();
                }else{
                    var var_tel_emp_per = "";
                    var var_ind_emp_per = "";
                }

                if($('#tel_mob_per').val() != ""){
                    var var_tel_mob_per = $('#tel_mob_per').val();
                    var var_ind_mob_per = $('#ind_mob_per option:selected').text();
                }else{
                    var var_tel_mob_per = "";
                    var var_ind_mob_per = "";
                }

                if($('#tel_per').val() != ""){
                    var var_tel_per = $('#tel_per').val();
                    var var_ind_per = $('#ind_per option:selected').text();
                }else{
                    var var_tel_per = "";
                    var var_ind_per = "";
                }

                $.post(
                    "./json/add_ens.php?_=" + Date.now(),
                    {
                        nom_per: $('#nom_per').val(),
                        prenom_per: $('#prenom_per').val(),
                        rue_per: $('#rue_per').val(),
                        id_cmn: $('#id_cmn').val(),
                        initial_per: $('#initial_per').val(),
                        genre_per: $('#genre_per').val(),
                        date_naissance_per: $('#date_naissance_per').val(),
                        email_per: $('#email_per').val(),
                        email_2_per: $('#email_2_per').val(),
                        remarque_ens_per: $('#remarque_ens_per').val(),
                        no_avs_per: $('#no_avs_per').val(),
                        no_persiska_per: $('#no_persiska_per').val(),
                        tel_emp_per: var_tel_emp_per,
                        ind_emp: var_ind_emp_per,
                        tel_mob_per: var_tel_mob_per,
                        ind_mob: var_ind_mob_per,
                        tel_per: var_tel_per,
                        ind_tel: var_ind_per,
                    },function (data) {
                        if(data.reponse){
                            $('#fct_brg_dip').css('display','block');
                            $('#btn_modif_ens').css('display','block');
                            $('#submit_conf').css('display','none');
                            $('#id_per').attr('id_per',data.id);
                        }
                    }
                ).fail(function () {
                    $('#content_error').html("<strong>Erreur</strong> Un problème est survenu avec l'envoi du formulaire");
                    $('#msg_error').slideDown();
                });


            }
        }
    )
});