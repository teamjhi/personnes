$(function () {
    $dateHaveNoEnd = false;
    $date_debut = $('#date_debut_fct');
    $date_fin = $('#date_fin_fct');
    $isErrorFctShown = false;
    $NbIdFct = 0;

    $(".datepicker").datepicker({
        format: "dd.mm.yyyy",
        weekStart: 1,
        language: "fr",
        forceParse: false,
        autoclose: true,
        todayHighlight: true
    });

    //Fonction
    $('#btn_add_fct').on('click', function () {
        if($("#date_debut_fct").val() == "") {
            $date_debut.css('background-color', '#f2dede');
            $date_debut.css('border-color', '#a94442');
        }else {
            $.post(
                "./json/add_fct_per.json.php",
                {
                    id_per: $('#id_per').attr('id_per'),
                    date_debut: $("#date_debut_fct").val(),
                    date_fin: $('#date_fin_fct').val(),
                    id_fct: $('#id_fct').val(),
                    nom_fct: $('#id_fct option:selected').attr('nom_fct')
                },
                function (data) {
                    if (data.data.date_fin == "" && data.data.date_debut != "") {
                        $dateHaveNoEnd = true;
                    }

                        if ($dateHaveNoEnd) {
                            data.data.date_fin = "-";
                            $('<tr id="fct_per_' + data.id + '">' +
                                '<td>' + data.data.date_debut + '</td><td>-</td>' +
                                '<td>' + data.data.nom_fct + '</td>' +
                                '<td><button type="button" class="btn btn-xs btn-danger btn_remove_fct" id_per_fct="' + data.id + '">\n' +
                                '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\n' +
                                '</button></td>' +
                                '<td></td>' +
                                '</tr>').insertBefore('.form_line');
                            resetFctDate();
                        } else {
                            if (!dateInspector()) {
                                $date_debut.css('background-color', '#f2dede');
                                $date_debut.css('border-color', '#a94442');
                                $date_fin.css('background-color', '#f2dede');
                                $date_fin.css('border-color', '#a94442');
                                if (!$isErrorFctShown) {
                                    $('#error_show_fct').append('<label class="error error_date_before"><div class="col-sm-12 alert alert-danger alert_nom" role="alert">\n' +
                                        '  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>\n' +
                                        '  <span class="sr-only">Error:</span>\n' +
                                        '  La date de début doit être inférieur à la date de fin \n' +
                                        '</div></label>')
                                    $isErrorFctShown = true;
                                }
                            } else {
                                $('<tr id="fct_per' + data.id + '">' +
                                    '<td>' + data.data.date_debut + '</td><td>' + data.data.date_fin + '</td>' +
                                    '<td>' + data.data.nom_fct + '</td>' +
                                    '<td><button type="button" class="btn btn-xs btn-danger btn_remove_fct" id_per_fct="' + data.id + '">\n' +
                                    '<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>\n' +
                                    '</button></td>' +
                                    '<td></td>' +
                                    '</tr>').insertBefore('.form_line');
                                resetFctDate();
                            }
                        }

                }
            );
        }
    });//FIN Fonction


    function dateInspector(){
        var start= $("#date_debut_fct").datepicker("getDate");
        var end= $("#date_fin_fct").datepicker("getDate");
        days = (end - start);
        if(days > 0 || $dateHaveNoEnd)
            return true;
        else
            return false;
    }

    function resetFctDate(){
        $date_debut.val('');
        $date_fin.val('');
        $('#ref_fct').val(1);
        $date_fin.css('border-color','');
        $date_debut.css('border-color','');
        $date_debut.css('background-color', '');
        $date_fin.css('background-color', '');
        $('.error_date_before').remove();
        $isErrorFctShown = false;
        $dateHaveNoEnd = false;
        $NbIdFct++;
    }

    $('#btn_cancel_fct').on('click', function () {
        resetFctDate();
    })

    $('#fonctions').on('click','.btn_remove_fct', function () {
        $.post(
            "./json/del_fct_per.json.php",
            {
                id_per_fct: $(this).attr('id_per_fct')
            },function(data){
                console.debug(data.id);
                $('#fct_per_'+data.data.id_per_fct).remove()
            }
        )
    });





//ajouter et effacer les branches

    $('#btn_add_brn').on('click', function () {
        if ($('#id_brg option:selected').text() == '') {
            $('#id_brg').css('border-color', '#a94442');
            $('#id_brg').css('background-color', '#f2dede');
        }else{
            $.post(
                "./json/add_brg.json.php",
                {
                    id_brg: $('#id_brg').val(),
                    id_per: $('#id_per').attr('id_per'),
                    nom_brg: $('#id_brg option:selected').attr('nom_brg')
                },
                function (data) {


                    $('#id_brg').css('border-color', '');
                    $('#id_brg').css('background-color', '');
                    $('<tr id="brg_per_' + data.id+ '">' +
                        '<td></td>' +
                        '<td>' + data.data.nom_brg + '</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-xs btn-danger btn_remove_brn" id_brg_per="' + data.id + '">' +
                        '   <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>' +
                        '</button>' +
                        '</td>' +
                        '</tr>').insertBefore('#header_brn');

                }
            );
        }
    });

    $('#branches').on('click', '.btn_remove_brn', function () {
        $.post(
            "./json/del_brg_per.json.php",
            {
                id_per_brg: $(this).attr('id_brg_per')
            }, function (data) {
                $('#brg_per_' + data.data.id_per_brg).remove()
            }
        )
    });

    //ajouter et effacer les diplômes

    $('#btn_add_dip').on('click',function () {
        $.post(
            "./json/add_dip.json.php",
            {
                id_dip:$('#id_dip').val(),
                id_per:$('#id_per').attr('id_per'),
                nom_dip: $('#id_dip option:selected').attr('nom_dip')
            },
            function(data){
                if($('#id_dip option:selected').text() == ''){
                    $('#id_dip').css('border-color', '#a94442');
                    $('#id_dip').css('background-color', '#f2dede');
                }else {
                    console.debug(data.data);
                    $('#id_dip').css('border-color', '');
                    $('<tr id="dip_per_' + data.id + '">' +
                        '<td></td>' +
                        '<td>' + data.data.nom_dip  + '</td>' +
                        '<td>' +
                        '<button type="button" class="btn btn-xs btn-danger btn_remove_dip" id_dip_per="' + data.id + '">' +
                        '   <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>' +
                        '</button>' +
                        '</td>' +
                        '</tr>').insertBefore('#header_dip');
                }
            }
        );
    });

    $('#diplomes').on('click','.btn_remove_dip', function () {
        $.post(
            "./json/del_dip_per.json.php",
            {
                id_dip_per: $(this).attr('id_dip_per')
            },function(data){
                $('#dip_per_'+data.data.id_dip_per).remove()
            }
        )
    });
})
