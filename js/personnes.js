$(function () {
    $('#modification').click(function () {
        console.log($('#nom_per').val());
        let donnee = [];
        $.each($('.modif_per'), function (key, value) {
            donnee.push(Array(value.id, value.value));
        });
        $.post("./json/update_ens.json.php",
            {
                tab: donnee,
                id_per: $('#id_per').val()
            }
        ).always(function () {
                $("#save_modif").fadeIn('slow');
                window.setTimeout(function() {
                    $("#save_modif").fadeTo(500, 0).slideUp(500, function(){
                        $(this).css('opacity', '100');
                    });
                }, 4000);
        })
    });


    $('#ajout').click(function () {
        console.log($('#nom_per').val());
        let donnee = [];
        $.each($('.modif_per'), function (key, value) {
            donnee.push(Array(value.id, value.value));
        });
        $.post("./json/add_per.json.php",
            {
                tab: donnee,
                id_per: $('#id_per').val()
            }
        ).always(function () {
            $("#save_modif").fadeIn('slow');
            window.setTimeout(function() {
                $("#save_modif").fadeTo(500, 0).slideUp(500, function(){
                    $(this).css('opacity', '100');
                });
            }, 4000);
        })
    });


    $("#select_commune").autocomplete({
        source: "./json/liste_cmn.json.php",
        minLength: 3,
        select: function(event, ui) {
            console.log(ui.item.id);
            $('#id_cmn').val(ui.item.id);
            // $("#form_edit_personne").submit();
        }
    });
});